<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
$con=new Consultas();
$paises=$con->get_country_list();


$id=0;
$port=null;
if($_GET["id"]!=null){
    $id=$_GET["id"];
    $port=$con->get_port($id);
    $port=$port[0];
    $codigo=$port["codigo"];
    $tipo=$port["tipo"];
    $nombre=$port["nombre"];
    $ciudad=$port["ciudad"];
    $pais=$port["pais"];
    $activo=($port["estatus"]==1)?1:0;

    //echo ">>>".$port["estatus"];
}


?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Insercion de Puertos</h3>
        <?php }else{?>
            <h3>Modificacion de Puertos</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=12 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

        <form role="form" action="ports.php" method="post" enctype="multipart/form-data">
            <?php if($id>0){ ?>
            <div class="form-group">
                <label for="id">Id</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="localidad">Codigo</label>
                <input type="text" class="form-control" id="codigo" name="codigo" placeholder="Codigo" value="<?php echo ($id>0)? $codigo:"" ?>" >
            </div>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo ($id>0)? $nombre:"" ?>" >
            </div>


            <div class="form-group">
                <label for="texto">Pais</label>
                <select class="form-control" name="paises" id="paises">
                    <?php
                    foreach($paises as $p){ ?>
                        <option value="<?php echo $p['pais']?>"><?php echo $p['pais']?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label for="fecha">Ciudad</label>
                <input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad" value="<?php echo ($id>0)? $ciudad:"" ?>" >
            </div>



            <div class="form-group">
                <label for="texto">Tipo de Puerto</label>
                <select class="form-control" name="tipo_puerto" id="tipo_puerto">
                    <option value="Puerto">Puerto</option>
                    <option value="Aeropuerto">Aeropuerto</option>
                </select>
            </div>

            <div class="checkbox">
                <label>
                    <input name="activo" type="checkbox" <?php echo ($id>0 && $activo==1)? "checked":""?> > Activo
                </label>
            </div>
            <button type="submit" class="btn btn-default">Enviar</button>
            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
            <input type="hidden" name="pais" value="<?php echo $pais?>">
        </form>
    </div>
</div>
<script type="text/javascript">
    var p='<?php echo $pais ?>';
    var pt='<?php echo $tipo ?>';
    $(document).ready(function(){
        $("#paises option[value='"+ p + "']").attr('selected', 'selected');
        $("#tipo_puerto option[value='"+ pt + "']").attr('selected', 'selected');
    });
</script>
</body>
</html>
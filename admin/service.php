<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
$con=new Consultas();
$paises=$con->get_country_list();

$id=0;
$port=null;
if($_GET["id"]!=null){
    $id=$_GET["id"];
    $serv=$con->get_service($id);
    $serv=$serv[0];
    $servicio=$serv["servicio"];
    $descripcion=$serv["descripcion"];
    $tipo_servicio=$serv["tipo_servicio"];
    $activo=($serv["estatus"]==1)?1:0;
    //$lst_paises=$serv["paises"];
    //echo ">>>".$port["estatus"];
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Insercion de Servicios</h3>
        <?php }else{?>
            <h3>Modificacion de Servicios</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=12 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

        <form role="form" action="services.php" method="post" enctype="multipart/form-data">
            <?php if($id>0){ ?>
            <div class="form-group">
                <label for="id">Id</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="localidad">Servicio</label>
                <input type="text" class="form-control" id="servicio" name="servicio" placeholder="Servicio" value="<?php echo ($id>0)? $servicio:"" ?>" >
            </div>
            <div class="form-group">
                <label for="nombre">Descripcion</label>
                <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripcion" value="<?php echo ($id>0)? $descripcion:"" ?>" >
            </div>
            <div class="form-group">
                <label for="texto">Tipo de Servicio</label>
                <select class="form-control" name="tipo_servicio" id="tipo_servicio">
                    <option value="1" <?php echo (substr($tipo_servicio,0,1)=="N")?"selected":"" ?>>Nacional</option>
                    <option value="2" <?php echo (substr($tipo_servicio,0,1)=="I")?"selected":"" ?>>Importación</option>
                    <option value="3" <?php echo (substr($tipo_servicio,0,1)=="E")?"selected":"" ?>>Exportación</option>
                    <option value="4" <?php echo (substr($tipo_servicio,0,1)=="S")?"selected":"" ?>>Servicios Adicionales</option>
                </select>
            </div>
            <div class="checkbox">
                <label>
                    <input name="activo" type="checkbox" <?php echo ($id>0 && $activo==1)? "checked":""?> > Activo
                </label>
            </div>
            <button type="submit" class="btn btn-default">Enviar</button>
            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
        </form>
    </div>
</div>
<script type="text/javascript">
    var p='<?php echo $pais ?>';
    var pt='<?php echo $tipo ?>';
    $(document).ready(function(){
        $("#paises option[value='"+ p + "']").attr('selected', 'selected');
        $("#tipo_puerto option[value='"+ pt + "']").attr('selected', 'selected');
    });
</script>
</body>
</html>



<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("funciones.php");
include_once("Consultas.php");
$con=new Consultas();


$id=0;
$port=null;

if($_GET["id"]!=null){
    $id=$_GET["id"];
    $ruta=$con->get_ruta($id)[0];

}


?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<script>
    var edit=0;
</script>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Salidas de la ruta</h3>
        <?php }else{?>
            <h3>Salidas de la ruta</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=15 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

            <?php if($id>0){ ?>
            <div class="form-group">
                <!--label for="id"></label-->
                <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $id ?>">
            </div>
            <?php  } ?>

            <div class="row">
                <!------------------------------------- ORIGEN ------------------------------------->
                <div class="col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="font-size: 16px; font-weight: bold">Origen de la ruta</div>
                        <div class="panel-body">
                            <div class="form-group ">
                                <label for="pais_o">Pais de Origen:</label>
                                <input type="text" class="form-control" id="pais_o" name="pais_o"  value="<?php echo $ruta['pais_o'] ?>" readonly >
                            </div>
                            <div class="form-group">
                                <label for="ciudad_o">Ciudad de Origen</label>
                                <input type="text" class="form-control" id="ciudad_o" name="ciudad_o"  value="<?php echo $ruta['ciudad_o'] ?>" readonly >
                            </div>
                            <div class="form-group">
                                <label for="puerto_o">Puerto de Origen</label>
                                <input type="text" class="form-control" id="puerto_o" name="puerto_o"  value="<?php echo $ruta['puerto_o'] ?>" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <!------------------------------------- DESTINO ------------------------------------->
                <div class="col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="font-size: 16px; font-weight: bold">Origen de la ruta</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="pais_d">Pais de Origen</label>
                                <input type="text" class="form-control" id="pais_d" name="pais_d"  value="<?php echo $ruta['pais_d'] ?>" readonly >
                            </div>
                            <div class="form-group">
                                <label for="ciudad_d">Ciudad de Origen</label>
                                <input type="text" class="form-control" id="ciudad_d" name="ciudad_d"  value="<?php echo $ruta['ciudad_d'] ?>" readonly >
                            </div>
                            <div class="form-group">
                                <label for="puerto_d">Puerto de Origen</label>
                                <input type="text" class="form-control" id="puerto_d" name="puerto_d"  value="<?php echo $ruta['puerto_d'] ?>" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="tipo_ruta">Tipo de Envio</label>
                            <input type="text" class="form-control" id="tipo_ruta" name="tipo_ruta"  value="<?php echo $ruta['tipo_ruta'] ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <!-------------------------------------------------------------------------------->
            <?php

            $pr_list=$con->get_providers_list();

            if($_GET["modo"]=="new"){
                agrega_salida_ruta();
            }else if($_GET["modo"]=="update"){
                //modifica_tarifa();
            }if($_GET["modo"]=="delete"){
                elimina_salida_ruta();
            }
            ?>
            <form class="form-horizontal" role="form" name="fr" method="get">

                <div class="form-group">
                    <label for="compania" class="col-lg-2 control-label" style="text-align: left">Proveedor</label>
                    <div class="col-lg-4">
                        <input type="hidden" name="id_salida" value="0">
                        <select class="form-control" name="compania" id="compania">
                            <option value=""></option>
                            <?php
                            foreach($pr_list as $p){ ?>
                                <option value="<?php echo $p['id']?>"><?php echo $p['compania']?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <!--label for="compania" class="col-lg-1 control-label" style="text-align: left">Currecny</label>
                    <div class="col-lg-2">
                        <select class="form-control" name="currecny" id="currecny">
                            <option value="DOL" <?php echo ($p['currecny']=='DOL')?'Selected':''; ?>">Dolar</option>
                            <option value="EUR" <?php echo ($p['currecny']=='EUR')?'Selected':''; ?>">Euro</option>
                        </select>
                    </div-->
                    <button type="button" class="btn btn-success" id="cancelar" disabled>Cancelar Edición</button>
                </div>




                <table class="table table-condensed table table-bordered" id="t_precios">
                    <thead>
                    <tr>
                        <td style="width: 20%; text-align: center">Fecha Base</td>
                        <td style="width: 20%; text-align: center">Periodo</td>
                        <td style="width: 25%; text-align: center">Dias Limite</td>
                        <td style="width: 25%; text-align: center">Lias Limite Documento</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="text" class="form-control input-sm text-right val" name="fecha_base" placeholder="aaaa-mm-dd"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="periodo"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="dias_limite"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="dias_limite_doc"></td>
                        <td rowspan="2"><button type="button" class="btn btn-success btn-sm" id="agregar" style="width: 100%">Guardar</button></td>
                    </tr>
                    </tbody>
                </table>
                <input type="hidden" id="modo"  name="modo" >
            </form>
            <hr>
            <div class="row">
                <div class="col-xs-12" id="contenedor">
                </div>
            </div>
                <!-------------------------------------------------------------------------------->
                <?php  /*
                <div class="checkbox">
                    <label>
                        <input name="activo" type="checkbox" <?php echo ($id>0 && $activo==1)? "checked":""?> > Activo
                    </label>
                </div>
                <button type="submit" class="btn btn-default">Enviar</button>
                <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">*/ ?>
        </div>
    </div>
    <script type="text/javascript">



    $(document).ready(function(){
        $(".val ").each(function(){
            $(this).val('0');
        });
        $(".val ").click(function(){
            if($(this).val()=='0'){
                $(this).val('');
            }
        });
        $(".val ").focusout(function(){
            if($(this).val()=='' || $(this).val()=='0'){
                $(this).val('0')
            }
            var v=parseFloat($(this).val());
            if(isNaN(v)){
                //alert('r');
                $(this).val('0');
            }
        });
        $.ajax({
            url: "tabla_salidas_ruta.php",
            type: "GET",
            data: {id_ruta:'<?php echo $id ?>'}
        }).done(function( html ) {
            $( "#contenedor").empty().append( html );
        });

    });


    $("#agregar").click(function(){
        if(document.fr.id_salida.value=="0"){
            //alert(document.fr.id_tarifa.value);
            agrega();
        }else{
            //alert("M"+document.fr.id_tarifa.value);
            modifica();
        }
    });

    function agrega(){
        var idpr=$("#compania option:selected").val();
        var exist=0;
        for(i=0; i<ids.length;i++){
            if(ids[i]==idpr){
                //alert(ids.length+"-"+ids[i]+ "-" +idpr);
                exist=1;
            }
        }


        if($("#compania option:selected").val()!='' && exist==0){
        $.ajax({
            url: "tabla_salidas_ruta.php",
            type: "GET",
            data: {
                id_ruta:<?php echo $id ?>,
                id_proveedor:idpr,
                fecha_base:document.fr.fecha_base.value,
                periodo:document.fr.periodo.value,
                dias_limite:document.fr.dias_limite.value,
                dias_limite_doc:document.fr.dias_limite_doc.value,
                modo:'new'}
        }).done(function( html ) {
            document.fr.fecha_base.value="";
            document.fr.periodo.value="0";
            document.fr.dias_limite.value="0";
            document.fr.dias_limite_doc.value="0";
            $( "#contenedor").empty().append(html);
        });
        }else{
            if(exist==1){
                alert("Este proveedor ya tiene una tarifa asignada para esta ruta!");
            }else{
                alert("Debe indicar un proveedor");
            }
        }

    }

    function modifica(){
        var idpr=$("#compania option:selected").val();
        var exist=0;

        if($("#compania option:selected").val()!='' && exist==0){
            $.ajax({
                url: "tabla_salidas_ruta.php",
                type: "GET",
                data: {
                    id_salida:document.fr.id_salida.value,
                    id_ruta:<?php echo $id ?>,
                    id_proveedor:idpr,
                    fecha_base:document.fr.fecha_base.value,
                    periodo:document.fr.periodo.value,
                    dias_limite:document.fr.dias_limite.value,
                    dias_limite_doc:document.fr.dias_limite_doc.value,
                    modo:'update'}
            }).done(function( html ) {
                document.fr.fecha_base.value="";
                document.fr.periodo.value="0";
                document.fr.dias_limite.value="0";
                document.fr.dias_limite_doc.value='0';
                edit=0;
                $("#contenedor").empty().append(html);
            });
        }
    }





    $("#cancelar").click(function(){
        document.fr.fecha_base.value="";
        document.fr.periodo.value="0";
        document.fr.dias_limite.value="0";
        document.fr.dias_limite_doc.value="0";
        $("#cancelar").attr("disabled",true);
        $("#compania").attr("disabled",false);
        $("#agregar").attr("disabled",true);
        $("#compania").val(0);
            edit=0;


    })


</script>

</body>
</html>

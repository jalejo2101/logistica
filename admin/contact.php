<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
$con=new Consultas();

$id=0;
$item=null;

if($_GET["id"]!=null){
    $id=$_GET["id"];
    $item=$con->get_contact($id);
    $item=$item[0];
    $company_name=$item['company_name'];
    $client_name=$item['client_name'];
    $mail=$item['mail'];
    $phone=$item['phone'];
    $subject=$item['subject'];
    $message=$item['message'];
    $date_c=$item['date_c'];

}


?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Informacion de Contactos</h3>
        <?php }else{?>
            <h3>Informacion de Contactos</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=8 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

        <form role="form" action="contacts.php" method="post" enctype="multipart/form-data">
            <?php if($id>0){ ?>
            <div class="form-group">
                <label for="id">Id</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            </div>
            <?php } ?>

            <div class="form-group">
                <label for="company_name">Company Name</label>
                <input readonly type="text" class="form-control" id="company_name" name="company_name" placeholder="company_name" value="<?php echo ($id>0)? $company_name:"" ?>" >
            </div>
            <div class="form-group">
                <label for="client_name">Client Name</label>
                <input readonly type="text" class="form-control" id="client_name" name="client_name" placeholder="client_name" value="<?php echo ($id>0)? $client_name:"" ?>" >
            </div>
            <div class="form-group">
                <label for="mail">Email</label>
                <input readonly type="text" class="form-control" id="mail" name="mail" placeholder="mail" value="<?php echo ($id>0)? $mail:"" ?>" >
            </div>
            <div class="form-group">
                <label for="phone">Phone</label>
                <input readonly type="text" class="form-control" id="phone" name="phone" placeholder="phone" value="<?php echo ($id>0)? $phone:"" ?>" >
            </div>
            <div class="form-group">
                <label for="phone">Date</label>
                <input readonly type="text" class="form-control" id="date_c" name="date_c" placeholder="date_c" value="<?php echo ($id>0)? $date_c:"" ?>" >
            </div>
            <div class="form-group">
                <label for="mail">Subject</label>
                <input readonly type="text" class="form-control" id="subject" name="subject" placeholder="subject" value="<?php echo ($id>0)? $subject:"" ?>" >
            </div>
            <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control" rows="3" id="message" name="message" readonly><?php echo ($id>0)? $message:"" ?></textarea>
            </div>


            <button type="button" onclick="window.open('contacts.php','_self','')" class="btn btn-default">Regresar</button>
            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("input,textarea").css("background-color","white");
    });
</script>
</body>
</html>
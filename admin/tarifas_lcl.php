<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("funciones.php");
include_once("Consultas.php");
$con=new Consultas();


$id=0;
$port=null;

if($_GET["id"]!=null){
    $id=$_GET["id"];
    $ruta=$con->get_ruta($id)[0];


}


?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<script>
    var edit=0;
</script>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Tarifas de ruta LCL</h3>
        <?php }else{?>
            <h3>Tarifas de ruta LCL</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=15 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

            <?php if($id>0){ ?>
            <div class="form-group">
                <!--label for="id"></label-->
                <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $id ?>">
            </div>
            <?php  } ?>

            <div class="row">
                <!------------------------------------- ORIGEN ------------------------------------->
                <div class="col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="font-size: 16px; font-weight: bold">Origen de la ruta</div>
                        <div class="panel-body">
                            <div class="form-group ">
                                <label for="pais_o">Pais de Origen:</label>
                                <input type="text" class="form-control" id="pais_o" name="pais_o"  value="<?php echo $ruta['pais_o'] ?>" readonly >
                            </div>
                            <div class="form-group">
                                <label for="ciudad_o">Ciudad de Origen</label>
                                <input type="text" class="form-control" id="ciudad_o" name="ciudad_o"  value="<?php echo $ruta['ciudad_o'] ?>" readonly >
                            </div>
                            <div class="form-group">
                                <label for="puerto_o">Puerto de Origen</label>
                                <input type="text" class="form-control" id="puerto_o" name="puerto_o"  value="<?php echo $ruta['puerto_o'] ?>" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <!------------------------------------- DESTINO ------------------------------------->
                <div class="col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="font-size: 16px; font-weight: bold">Origen de la ruta</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="pais_d">Pais de Origen</label>
                                <input type="text" class="form-control" id="pais_d" name="pais_d"  value="<?php echo $ruta['pais_d'] ?>" readonly >
                            </div>
                            <div class="form-group">
                                <label for="ciudad_d">Ciudad de Origen</label>
                                <input type="text" class="form-control" id="ciudad_d" name="ciudad_d"  value="<?php echo $ruta['ciudad_d'] ?>" readonly >
                            </div>
                            <div class="form-group">
                                <label for="puerto_d">Puerto de Origen</label>
                                <input type="text" class="form-control" id="puerto_d" name="puerto_d"  value="<?php echo $ruta['puerto_d'] ?>" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="tipo_ruta">Tipo de Envio</label>
                            <input type="text" class="form-control" id="tipo_ruta" name="tipo_ruta"  value="<?php echo $ruta['tipo_ruta'] ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <!-------------------------------------------------------------------------------->
            <?php

            $pr_list=$con->get_providers_list();

            if($_GET["modo"]=="new"){
                agrega_tarifa_lcl();
            }else if($_GET["modo"]=="update"){
                //modifica_tarifa();
            }if($_GET["modo"]=="delete"){
                elimina_tarifa_lcl();
            }
            ?>
            <form class="form-horizontal" role="form" name="fr" method="get">
                <div class="form-group">
                    <label for="compania" class="col-lg-2 control-label" style="text-align: left">Proveedor</label>
                    <div class="col-lg-4">
                        <input type="hidden" name="id_tarifa" value="0">
                        <select class="form-control" name="compania" id="compania">
                            <option value=""></option>
                            <?php
                            foreach($pr_list as $p){ ?>
                                <option value="<?php echo $p['id']?>"><?php echo $p['compania']?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <label for="compania" class="col-lg-1 control-label" style="text-align: left">Currecny</label>
                    <div class="col-lg-2">
                        <select class="form-control" name="currecny" id="currecny">
                            <option value="DOL" <?php echo ($p['currecny']=='DOL')?'Selected':''; ?>">Dolar</option>
                            <option value="EUR" <?php echo ($p['currecny']=='EUR')?'Selected':''; ?>">Euro</option>
                        </select>
                    </div>
                    <button type="button" class="btn btn-success" id="cancelar" disabled>Cancelar Edición</button>
                </div>

                <table class="table table-condensed table table-bordered" id="t_precios">
                    <thead>
                    <tr>

                        <td style="width: 15%; text-align: center">Port Expenses Min</td>
                        <td style="width: 15%; text-align: center">Port Expenses </td>

                        <td style="width: 15%; text-align: center">Cargo Inspection Min</td>
                        <td style="width: 15%; text-align: center">Cargo Inspection</td>

                        <td style="width: 15%; text-align: center">Consolidation Min</td>
                        <td style="width: 15%; text-align: center">Consolidation</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="text" class="form-control input-sm text-right val" name="port_min"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="port"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="cargo_min"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="cargo"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="consolidation_min"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="consolidation"></td>
                    </tr>
                    </tbody>
                    <thead>
                    <tr>
                        <td style="text-align: center" colspan="2">Sea Freight</td>

                        <td style="text-align: center">Wharehouse Min</td>
                        <td style="text-align: center">Wharehouse</td>

                        <td style="text-align: center">SED</td>
                        <td style="text-align: center">BL</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="2"><input type="text" class="form-control input-sm text-right val" name="flete"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="wharehouse_min"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="wharehouse"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="sed"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="bl"></td>
                        <td rowspan="4"><button type="button" class="btn btn-success btn-sm" id="agregar" style="width: 100%">Guardar</button></td>
                    </tr>
                    </tbody>
                </table>
                <input type="hidden" id="modo"  name="modo" >
            </form>
            <hr>
            <div class="row">
                <div class="col-xs-12" id="contenedor">

                </div>
            </div>
                <!-------------------------------------------------------------------------------->
                <?php  /*
                <div class="checkbox">
                    <label>
                        <input name="activo" type="checkbox" <?php echo ($id>0 && $activo==1)? "checked":""?> > Activo
                    </label>
                </div>
                <button type="submit" class="btn btn-default">Enviar</button>
                <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">*/ ?>
        </div>
    </div>
    <script type="text/javascript">



    $(document).ready(function(){
        $(".val ").each(function(){
            $(this).val('0,00');
        });
        $(".val ").click(function(){
            if($(this).val()=='0,00'){
                $(this).val('');
            }
        });
        $(".val ").focusout(function(){
            if($(this).val()=='' || $(this).val()=='0'){
                $(this).val('0,00')
            }
            var v=parseFloat($(this).val());
            if(isNaN(v)){
                //alert('r');
                $(this).val('0,00');
            }
        });

        $.ajax({
            url: "tabla_tarifas_lcl.php",
            type: "GET",
            data: {id_ruta:'<?php echo $id ?>'}
        }).done(function( html ) {
            $( "#contenedor").empty().append( html );
        });

    });


    $("#agregar").click(function(){
        if(document.fr.id_tarifa.value=="0"){
            //alert(document.fr.id_tarifa.value);
            agrega();
        }else{
            //alert("M"+document.fr.id_tarifa.value);
            modifica();
        }
    });

    function agrega(){
        var idpr=$("#compania option:selected").val();
        var exist=0;
        for(i=0; i<ids.length;i++){
            if(ids[i]==idpr){
                //alert(ids.length+"-"+ids[i]+ "-" +idpr);
                exist=1;
            }
        }


        if($("#compania option:selected").val()!='' && exist==0){
        $.ajax({
            url: "tabla_tarifas_lcl.php",
            type: "GET",
            data: {
                id_ruta:<?php echo $id ?>,
                id_proveedor:idpr,
                flete:document.fr.flete.value,
                port_min:document.fr.port_min.value,
                port:document.fr.port.value,
                cargo_min:document.fr.cargo_min.value,
                cargo:document.fr.cargo.value,
                consolidation_min:document.fr.consolidation_min.value,
                consolidation:document.fr.consolidation.value,
                wharehouse_min:document.fr.wharehouse_min.value,
                wharehouse:document.fr.wharehouse.value,
                sed:document.fr.sed.value,
                bl:document.fr.bl.value,
                currecny:$("#currecny").val(),
                modo:'new'}
        }).done(function( html ) {
            document.fr.flete.value="0,00";
            document.fr.port_min.value="0,00";
            document.fr.port.value="0,00";
            document.fr.cargo_min.value="0,00";
            document.fr.cargo.value='0,00';
            document.fr.consolidation_min.value='0,00';
            document.fr.consolidation.value='0,00';
            document.fr.wharehouse_min.value='0,00';
            document.fr.wharehouse.value='0,00';
            document.fr.sed.value='0,00';
            document.fr.bl.value='0,00';
            $("#currecny").val('DOL');
            $( "#contenedor").empty().append(html);
        });
        }else{
            if(exist==1){
                alert("Este proveedor ya tiene una tarifa asignada para esta ruta!");
            }else{
                alert("Debe indicar un proveedor");
            }
        }

    }

    function modifica(){
        var idpr=$("#compania option:selected").val();
        var exist=0;

        if($("#compania option:selected").val()!='' && exist==0){
            $.ajax({
                url: "tabla_tarifas_lcl.php",
                type: "GET",
                data: {
                    id_tarifa:document.fr.id_tarifa.value,
                    id_ruta:<?php echo $id?>,
                    flete:document.fr.flete.value,
                    port_min:document.fr.port_min.value,
                    port:document.fr.port.value,
                    cargo_min:document.fr.cargo_min.value,
                    cargo:document.fr.cargo.value,
                    consolidation_min:document.fr.consolidation_min.value,
                    consolidation:document.fr.consolidation.value,
                    wharehouse_min:document.fr.wharehouse_min.value,
                    wharehouse:document.fr.wharehouse.value,
                    sed:document.fr.sed.value,
                    bl:document.fr.bl.value,
                    currecny:$("#currecny").val(),
                    modo:'update'}
            }).done(function( html ) {
                document.fr.flete.value="0,00";
                document.fr.port_min.value="0,00";
                document.fr.port.value="0,00";
                document.fr.cargo_min.value="0,00";
                document.fr.cargo.value='0,00';
                document.fr.consolidation_min.value='0,00';
                document.fr.consolidation.value='0,00';
                document.fr.wharehouse_min.value='0,00';
                document.fr.wharehouse.value='0,00';
                document.fr.sed.value='0,00';
                document.fr.bl.value='0,00';
                $("#currecny").val('DOL');
                $("#compania").val(0);
                $("#cancelar").attr("disabled",true);
                $("#compania").attr("disabled",false);
                edit=0;
                $("#contenedor").empty().append(html);
            });
        }
    }





    $("#cancelar").click(function(){
        document.fr.flete.value="0,00";
        document.fr.port_min.value="0,00";
        document.fr.port.value="0,00";
        document.fr.cargo_min.value="0,00";
        document.fr.cargo.value='0,00';
        document.fr.consolidation_min.value='0,00';
        document.fr.consolidation.value='0,00';
        document.fr.wharehouse_min.value='0,00';
        document.fr.wharehouse.value='0,00';
        document.fr.sed.value='0,00';
        document.fr.bl.value='0,00';
        $("#currecny").val('DOL');
        $("#cancelar").attr("disabled",true);
        $("#compania").attr("disabled",false);
        $("#compania").val(0);
            edit=0;


    })


</script>
</body>
</html>

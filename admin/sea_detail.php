<?php
include_once("Consultas.php");
include_once("funciones.php");
$con=new Consultas();
if(isset($_GET["tipo"])){
    if($_GET["tipo"]=="new"){
        agrega_sea_detail();
    }else{
        update_sea_detail();
    }
}


if(isset($_GET["bl_number"])){
    $id=$_GET["id"];
    if(isset($_GET["mode"])){
        if($_GET["mode"]==delete){
            elimina_sea_detail($id);
        }
    }
    $bl=$_GET["bl_number"];
    $sea_detail=$con->get_sea_tracking_detail($bl);
}
?>
        <style>
            #ui-datepicker-div{
                font-size: 10px;
            }
        </style>
        <form name="det">
            <table class="table">
                <thead>
                <tr style="background: #ffffff">
                    <td style="width: 20%; text-align: center"">LOCATION</td>
                    <td style="width: 25%; text-align: center; min-width: 155px">DESCRIPTION</td>
                    <td style="width: 15%; text-align: center">DATE</td>
                    <td style="width: 15%; text-align: center; min-width: 155px">VESSEL</td>
                    <td style="width: 15%; text-align: center; min-width: 155px">VOYAGE</td>
                    <td style="width: 5%; text-align: center"></td>
                    <td style="width: 5%; text-align: center"></td>
                </tr>
                </thead>
                </body>
                    <td><input class="form-control input-sm" type="text" id="location" name="location"></td>
                    <td><input class="form-control input-sm" type="text" id="description" name="description"></td>
                    <td><input class="form-control input-sm" type="date" id="" name="date"></td>
                    <td><input class="form-control input-sm" type="text" id="vessel" name="vessel"></td>
                    <td><input class="form-control input-sm" type="text" id="voyage" name="voyage"></td>
                    <td><button type="button" class="btn btn-success btn-sm" style="width: 100%" onclick="guardar()">G</button></td>
                    <td><button type="button" class="btn btn-danger btn-sm" style="width: 100%" onclick="cancel()">X</button></td>
                </tbody>
            </table>
            <input type="hidden" name="id">
            <input type="hidden" name="tipo" value="new">
        </form>
        <table class="table table-hover">
            <thead>
            <tr style="background: #9acfea">
                <td style="width: 20%; text-align: center"">LOCATION</td>
                <td style="width: 25%; text-align: center; min-width: 155px">DESCRIPTION</td>
                <td style="width: 15%; text-align: center">DATE</td>
                <td style="width: 15%; text-align: center; min-width: 155px">VESSEL</td>
                <td style="width: 15%; text-align: center; min-width: 155px">VOYAGE</td>
                <td style="width: 5%; text-align: center">M</td>
                <td style="width: 5%; text-align: center">E</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($sea_detail as $det){ ?>
                <tr>
                    <td><?php echo $det['location']?></td>
                    <td style="text-align: center"><?php echo $det['description']?></td>
                    <td><?php echo $det['date']?></td>
                    <td style="text-align: center"><?php echo $det['vessel']?></td>
                    <td style="text-align: center"><?php echo $det['voyage']?></td>
                    <?php $upd="'".$det['id']."','".$det['location']."','".$det['description']."','".$det['date']."','".$det['vessel']."','".$det['voyage']."'" ?>
                    <td style="text-align: center"><a href="javascript:edit(<?php echo $upd ?>)"><img src="img/edit_icon.png"></a></td>
                    <?php $param="bl_number=".$bl."&mode=delete&id=".$det['id']; ?>
                    <td style="text-align: center"><a href="javascript:detalles('<?php echo $param ?>')"><img src="img/delete_icon.png"></a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <script>
        $('#dep_date').datetimepicker();
        $('#arr_date').datetimepicker();
        function guardar(){
            loca=document.det.location.value;
            desc=document.det.description.value;
            date=document.det.date.value;
            vess=document.det.vessel.value;
            voya=document.det.voyage.value;
            id=document.det.id.value;
            bl='<?php echo $bl ?>';
            if(document.det.tipo.value=="new"){
                url="location="+loca+"&description="+desc+"&date="+date+"&vessel="+vess+"&voyage="+voya+"&bl_number="+bl+"&tipo=new";
            }else{
                url="location="+loca+"&description="+desc+"&date="+date+"&vessel="+vess+"&voyage="+voya+"&id="+id+"&bl_number="+bl+"&tipo=update";
            }
            //alert(url);
            detalles(url);
        }
        function edit(id,loca,desc,date,vess,voya){
            document.det.location.value=loca;
            document.det.description.value=desc;
            document.det.date.value=date;
            document.det.vessel.value=vess;
            document.det.voyage.value=voya;
            document.det.id.value=id;
            document.det.tipo.value="update";
        }
        function cancel(){
            document.det.location.value="";
            document.det.description.value="";
            document.det.date.value="";
            document.det.vessel.value="";
            document.det.voyage.value="";
            document.det.id.value="";
            document.det.id.tipo.value="new";
        }

    </script>

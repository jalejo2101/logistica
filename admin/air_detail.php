<?php
include_once("Consultas.php");
include_once("funciones.php");
$con=new Consultas();
if(isset($_GET["tipo"])){
    if($_GET["tipo"]=="new"){
        agrega_air_detail();
    }else{
        update_air_detail();
    }
}


if(isset($_GET["awb_number"])){
    $id=$_GET["id"];
    if(isset($_GET["mode"])){
        if($_GET["mode"]==delete){
            elimina_air_detail($id);
        }
    }
    $awb=$_GET["awb_number"];
    $air_detail=$con->get_air_tracking_detail($awb);
}
?>
        <style>
            #ui-datepicker-div{
                font-size: 10px;
            }
        </style>
        <form name="det">
            <table class="table">
                <thead>
                <tr style="background: #ffffff">
                    <td style="width: 25%; text-align: center"">DEPARTURE AIRPORT</td>
                    <td style="width: 20%; text-align: center; min-width: 155px">DEPARTURE DATE</td>
                    <td style="width: 25%; text-align: center">ARRIVAL AIRPORT</td>
                    <td style="width: 20%; text-align: center; min-width: 155px">ARRIVAL DATE</td>
                    <td style="width: 5%; text-align: center"></td>
                    <td style="width: 5%; text-align: center"></td>
                </tr>
                </thead>
                </body>
                    <td><input class="form-control input-sm" type="text" id="dep_airport" name="dep_airport"></td>
                    <td><input class="form-control input-sm" type="date" id="" name="dep_date"></td>
                    <td><input class="form-control input-sm" type="text" id="arr_airport" name="arr_airport"></td>
                    <td><input class="form-control input-sm" type="date" id="" name="arr_date"></td>
                    <td><button type="button" class="btn btn-success btn-sm" style="width: 100%" onclick="guardar()">G</button></td>
                    <td><button type="button" class="btn btn-danger btn-sm" style="width: 100%" onclick="cancel()">X</button></td>
                </tbody>
            </table>
            <input type="hidden" name="id">
            <input type="hidden" name="tipo" value="new">
        </form>
        <table class="table table-hover">
            <thead>
            <tr style="background: #9acfea">
                <td style="width: 25%; text-align: center"">DEPARTURE AIRPORT</td>
                <td style="width: 20%; text-align: center; min-width: 155px">DEPARTURE DATE</td>
                <td style="width: 25%; text-align: center">ARRIVAL AIRPORT</td>
                <td style="width: 20%; text-align: center; min-width: 155px">ARRIVAL DATE</td>
                <td style="width: 5%; text-align: center">M</td>
                <td style="width: 5%; text-align: center">E</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($air_detail as $det){ ?>
                <tr>
                    <td><?php echo $det['dep_airport']?></td>
                    <td style="text-align: center"><?php echo $det['dep_date']?></td>
                    <td><?php echo $det['arr_airport']?></td>
                    <td style="text-align: center"><?php echo $det['arr_date']?></td>
                    <?php $upd="'".$det['id']."','".$det['dep_airport']."','".$det['dep_date']."','".$det['arr_airport']."','".$det['arr_date']."'" ?>
                    <td style="text-align: center"><a href="javascript:edit(<?php echo $upd ?>)"><img src="img/edit_icon.png"></a></td>
                    <?php $param="awb_number=".$awb."&mode=delete&id=".$det['id']; ?>
                    <td style="text-align: center"><a href="javascript:detalles('<?php echo $param ?>')"><img src="img/delete_icon.png"></a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <script>
        $('#dep_date').datetimepicker();
        $('#arr_date').datetimepicker();
        function guardar(){
            dep_a=document.det.dep_airport.value;
            dep_d=document.det.dep_date.value;
            arr_a=document.det.arr_airport.value;
            arr_d=document.det.arr_date.value;
            id=document.det.id.value;
            awb='<?php echo $awb ?>';
            if(document.det.tipo.value=="new"){
                url="dep_airport="+dep_a+"&dep_date="+dep_d+"&arr_airport="+arr_a+"&arr_date="+arr_d+"&awb_number="+awb+"&tipo=new";
            }else{
                url="dep_airport="+dep_a+"&dep_date="+dep_d+"&arr_airport="+arr_a+"&arr_date="+arr_d+"&id="+id+"&awb_number="+awb+"&tipo=update";
            }
            //alert(url);
            detalles(url);
        }
        function edit(id,dep_a,dep_d,arr_a,arr_d){
            document.det.dep_airport.value=dep_a;
            document.det.dep_date.value=dep_d;
            document.det.arr_airport.value=arr_a;
            document.det.arr_date.value=arr_d;
            document.det.id.value=id;
            document.det.tipo.value="update";
        }
        function cancel(){
            document.det.dep_airport.value="";
            document.det.dep_date.value="";
            document.det.arr_airport.value="";
            document.det.arr_date.value="";
            document.det.id.value="";
            document.det.id.tipo.value="new";
        }
        /*function update(){
            dep_a=document.det.dep_airport.value;
            dep_d=document.det.dep_date.value;
            arr_a=document.det.arr_airport.value;
            arr_d=document.det.arr_date.value;
            id=document.det.id.value;
            url="dep_airport="+dep_a+"&dep_date="+dep_d+"&arr_airport="+arr_a+"&arr_date="+arr_d+"&id="+id+"&tipo=update";
            alert(url);
            detalles(url);
        }*/



    </script>


<?php
    require_once '../include/header.php';
?>

<script>
    $(document).ready(function() {
        //$('nav#menu-top ul.main-sect > li:nth-child(2) a').addClass("page-on");
    });
</script>
<!-- =================== CONTENIDO  =================== -->         

        <div id="content">

            <div id="main-content-full" class="noaliados">
                <section class="tracking-box">
                    <h1>Track Shipment</h1>
                    <h2 class="air">Air Tracking </h2>

                    
                    <table class="track-item">
                        <tr>
                            <td>AWB NUMBER</td>
                            <td>057-6124430</td>
                        </tr>
                        <tr>
                            <td>Shipped From</td>
                            <td>SOFIA, BULGARY</td>
                        </tr>
                        <tr>
                            <td>Destination</td>
                            <td>GUAYAQUIL, ECUADOR</td>
                        </tr>
                        <tr>
                            <td>Number of Package</td>
                            <td>5</td>
                        </tr>
                    </table>
                    
                    
                    
                    
                    <table class="track-logs">
                        <thead>
                            <tr>
                                <th>Departure Airport</th>
                                <th>Departure date</th>
                                <th>Arrival Airport</th>
                                <th>ArrivalDate</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Sofia, Bulgary</td>
                                <td>May 10th/2014 12:00 hrs</td>
                                <td>Amsterdam, NL</td>
                                <td>MAy 10th/2014 20:00 hrs</td>
                            </tr>
                            <tr>
                                <td>Amsterdam, NL</td>
                                <td>May 10th/2014 12:00 hrs</td>
                                <td>Guayaquil, Ecuador</td>
                                <td>MAy 12th/2014 20:00 hrs</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <table class="track-item">
                        <tr>
                            <td>DELIVERED TO CONSIGNEE</td>
                            <td>MAY 13TH/2014 14:00 HRS</td>
                        </tr>
                        <tr>
                            <td>REMARKS</td>
                            <td>DELIVERED 5 PIECES</td>
                        </tr>
                    </table>
                    
                    
                    <a class="print" href="#">Print</a>
                </section>
                
            </div>
        </div>


<!-- =================== FOOTER  ====================== -->   

<?php
    require_once '../include/footer.php';
?>
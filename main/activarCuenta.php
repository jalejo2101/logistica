<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

<?php

    require_once '../include/header.php';
    $reg=false;
    $con= new Consultas();
    $ok=false;

    if(isset($_GET["m"])){
        $usr_mc= $con->get_mail_and_code($_GET["m"], $_GET["cod"]);
        //var_dump($usr_mc);
        if(count($usr_mc)>0){
            activar_usuario($_GET["m"]);
            $ok=true;
        }
    }
?>

<!-- =================== CONTENIDO  =================== -->
        <div id="content">
            <div id="main-content-full" class="noaliados">
                <section class="tracking-box">
                    <h1><?php lang("ACCOUNT ACTIVATION","ACTIVACION DE CUENTA" )?> </h1>
                    <div id="register-panel">
                        <!--------------------------------------------------------------------------->
                        <!--------------------------------------------------------------------------->
                        <section style="display:block">
                            <?php if($ok){?>
                            <ul class="register">
                                <li class="reg1 first"> <label><?php lang("Your account has been activated,.","Su Cuenta ha sido activada correctamente." )?> </label>
                                <li class="reg1 first"> <label>Click <a href="#" onclick="show('.login-box');" style="color: #3278b3"><?php lang("here","aquí" )?> </a><?php lang("to login to your account","para iniciar sesión en su cuenta" )?> </label>
                            </ul>
                            <?php }else{?>
                                <ul class="register">
                                    <li class="reg1 first"> <label><?php lang("Cuenta ya activa o no existe","Cuenta ya activa o no existe" )?> </label></li>
                                </ul>
                            <?php } ?>
                        </section>
                        <!--------------------------------------------------------------------------->
                    </div>
                </section>
            </div>
        </div>
<script>


</script>

<!-- =================== FOOTER  ====================== -->   


<?php
    require_once '../include/footer.php';
?>

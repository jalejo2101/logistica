<?php session_start();?>
<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

<?php
if(!isset($_SESSION['user'])){
    header ("location:index.php");
}

require_once '../include/header.php';
$con=new Consultas();

if(isset($_SESSION['user'])){
    $user=$con->get_usuario($_SESSION['user']);
    $user=$user[0];
}

if($_POST){
    $pass=$user["pass"];
    $old_pass=md5($_POST['old_pass']);
    $new_pass=md5($_POST['new_pass']);
    if($pass==$old_pass || $pass==$_POST['old_pass']){
        cambiar_password($_SESSION['user'],$new_pass);
        echo "<script>alert('La contraseña ha sido cambiada con éxito.\\nThe password has been changed successfully.')</script>";
        echo "<script>window.open('index.php','_self','')</script>";
    }else{
        echo "<script>alert('Introduzca la contraseña anterior y la nueva.\\n\\nEnter your old and new passwords.')</script>";
    }
    if($_POST['reset']=='true'){
        unset($_SESSION['user']);

    }
}
if($_GET){
    $ref=$_GET['ref'];
    $user=$con->get_usuario_id($ref);
    $user=$user[0];
    $_SESSION['user']=$user['mail'];
    $pass_md5=$_GET['psw'];
    $b=true;
}
?>
<!-- =================== CONTENIDO  =================== -->         

        <div id="content">

            <div id="main-content-full" class="noaliados">
                
                
                <section class="tracking-box">
                    <h1><?php lang("CHANGE PASSWORD","CAMBIAR CLAVE");?></h1>
                    
                    <ul class="profile-nav">
                        <li><a href="myprofile.php"><img src="../img/icon-prof-off.png"><p><?php lang("MY PROFILE","MI PERFIL");?></p></a></li>
                        <li><a href="shipment.php"><img src="../img/icon-ship-off.png"><p><?php lang("SHIPMENT","EMBARQUES");?></p></a></li>
                        <li><a><img src="../img/icon-pass-on.png"><p><?php lang("CHANGE PASSWORD","CAMBIAR CLAVE");?></p></a></li>
                    </ul>
                    
                    <div id="changepass-panel">
                        <section class="reg-step01">
                            <form name="chgpass" method="post" action="chgpass.php">
                            <ul class="register">
                                <li class="reg1"> <label><?php lang("Old password","Clave actual");?></label>
                                    <?php if($b){ ?>
                                        <input type="password" name="old_pass"  value="<?php echo $pass_md5 ?>" readonly>
                                    <?php }else{ ?>
                                        <input type="password" name="old_pass" placeholder="<?php lang("Enter old password","Ingrese clave actual");?>">
                                    <?php } ?>
                                </li>
                                <li class="reg1"> <label><?php lang("New password","Nueva clave");?></label><input type="password" name="new_pass" placeholder="<?php lang("Enter new password","Ingrese nueva clave");?>"> </li>
                                <li class="reg1"> <label><?php lang("Confirm New password","Confirmar clave");?></label><input type="password" name="rep_pass" placeholder="<?php lang("Confirm New password","Confirmar clave");?>"> </li>
                            </ul>
                            <a onclick="enviarPass();" class="edit-saveESP"><?php lang("Save","Guardar");?></a>
                            <input type="hidden" name="reset" <?php if($b) echo 'value="true"'?>>
                            </form>
                        </section>
                    
                    </div>
                </section>
                
            </div>
        </div>
<script>
    function enviarPass(){
        if(document.chgpass.new_pass.value==document.chgpass.rep_pass.value){
            document.chgpass.submit();
        }else{
            alert('Los Password deben ser iguales \nThe Password should be equal');
        }

    }

    function message(msg){
        alert(msg);
    }
</script>


<!-- =================== FOOTER  ====================== -->   

<?php
    require_once '../include/footer.php';
?>
<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

    <h1>COTIZACION CARGA PELIGROSA</h1>

    <ul class="register">
        <li class="reg1">
            <label><?php lang("Embarcador","Embarcador" )?> </label>
            <input type="text" name="name" required>
        </li>

        <li class="reg2 first">
            <label><?php lang("Fecha de Solicitud","Fecha de Solicitud")?> </label>
            <input type="text" name="pickup" required>
        </li>
        <li class="reg2">
            <label><?php lang("Pais","Pais" )?> </label>
            <input type="text" name="zip" required>
        </li>

        <li class="reg2 first">
            <label><?php lang("Ciudad","Ciudad" )?> </label>
            <input type="text" name="phone" required>
        </li>
        <li class="reg2">
            <label><?php lang("Telefono de Contacto","Telefono de Contacto" )?> </label>
            <input type="text" name="mobil" required>
        </li>

        <li class="reg1">
            <label><?php lang("Direccion","Direccion" )?> </label>
            <input type="text" name="mail" required>
        </li>

        <li class="reg1">
            <label><?php lang("Email","Email" )?> </label>
            <input type="text" name="loading" >
        </li>

        <li class="reg1">
            <label><?php lang("Direccion de Entrega","Direccion de Entrega" )?> </label>
            <input type="text" name="discharge" >
        </li>

        <li class="reg2 first">
            <label><?php lang("Pais","Pais" )?> </label>
            <input type="text" name="commodity" >
        </li>
        <li class="reg2">
            <label><?php lang("Ciudad","Ciudad" )?> </label>
            <input type="text" name="fork" >
        </li>

        <li class="reg2 first">
            <label><?php lang("Telefono de Contacto","Telefono de Contacto" )?> </label>
            <input type="text" name="stack" style="margin-top: 6px" >
        </li>


        <li class="reg2">
            <label><?php lang("Tipo de Contenedor","Tipo de Contenedor" )?> </label>
            <select name="type_cont" style="margin-top: 8px; width: 330px; margin-bottom: 0px;">
                <option value="20">20</option>
                <option value="40">40</option>
                <option value="40hc">40HQ</option>
            </select>
        </li>

        <li class="reg1 first">
            <label><?php lang("Nombre Comercial del Producto","Nombre Comercial del Producto" )?> </label>
            <input type="text" name="nombre_c" style="margin-top: 6px" >
        </li>
        <li class="reg1 first">
            <label><?php lang("Nombre Propio","Nombre Propio" )?> </label>
            <input type="text" name="nombre_p" style="margin-top: 6px" >
        </li>

        <h1 style="text-transform: none">Informacion de Carga</h1>

        <li class="reg2 first">
            <label><?php lang("Clase IMO / Division","Clase IMO / Division" )?> </label>
            <input type="text" name="clase_crg" >
        </li>
        <li class="reg2">
            <label><?php lang("UN Numero","Un Numero" )?> </label>
            <input type="text" name="un" >
        </li>

        <li class="reg1 first">
            <label><?php lang("Grupo de embalaje","Un Numero" )?> </label>
            <input type="text" name="grupo" >
        </li>


        <li class="reg2 first">
            <label><?php lang("Punto de Inflamcion","Punto de Inflamcion" )?> </label>
            <input type="text" name="inflamcion" >
        </li>
        <li class="reg2">
            <label><?php lang("Empaque Interno","Empaque Interno" )?> </label>
            <input type="text" name="p_interno" >
        </li>


        <li class="reg2 first">
            <label><?php lang("Empaque Externo","Empaque Externo" )?> </label>
            <input type="text" name="p_externo" >
        </li>
        <li class="reg2">
            <label><?php lang("Cantidad de Bultos","Cantidad de Bultos" )?> </label>
            <input type="text" name="cantidad" >
        </li>

        <li class="reg2 first">
            <label><?php lang("Peso","Peso" )?> </label>
            <input type="text" name="peso" >
        </li>
        <li class="reg2">
            <label><?php lang("Volumen","Volumen" )?> </label>
            <input type="text" name="volumen" >
        </li>



    </ul>
    <table style="width: 100%">
        <tr>
            <td style="text-align: center">
                <a onclick="validar()" class="<?php lang("reg-next","reg-nextESP")?>" style="position: relative"><?php lang("Enviar","Enviar" )?>  </a>
            </td>
        </tr>
        <!--tr>
            <td style="text-align: center; height: 40px; vertical-align: bottom">
                <a href="" style="color: #00389D"><?php lang("Descarga de formato para calcular volumen de carga","Descarga de formato para calcular volumen de carga" )?> </a>
            </td>
        </tr-->
    </table>


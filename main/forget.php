<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php
$b=false;
if($_POST){
    $con=new Consultas();
    $mail=$_POST['mail'];
    $user=$con->get_usuario($mail);
    if(count($user)==0){
        //lang("<script>alert('The mail address does not exist.\n\n')</script>","<script>alert('')</script>");        
        echo "<script>alert('El correo electrónico no existe.\\nThe mail address does not exist.');</script>";
    }else{
        $b=true;
        $user=$user['0'];
        $cad=md5(rand(100,999).$user['mail']);
        cambiar_password($user['mail'],$cad);

        $destinatario = $mail; // "jalejo.f@gmail.com";
        $asunto = "Cambiar Clave / New Password";
        $cuerpo = '
                <html><body>
                <p style="font-family:verdana; font-size:13px">
                <img src="http://fcl-ecuador.com/Freighlogistics/img/logo.png">
                <br><br>

                Estimado cliente, el siguiente vinculo 
                <a href="http://fcl-ecuador.com/Freighlogistics/main/chgpass.php?ref='.$user["id"].'&psw='.$cad.'"  target="_blank">Click Aqu&iacute;</a> 
                le permitir&aacute; ingresar a la p&aacute;gina principal sin su clave, una vez  ah&iacute; de click en  "Mi cuenta" ,
                luego elija  "Cambio de clave"  y proceda a  ingresar la informaci&oacute;n de  la nueva clave.

                <br> <br>    



                Dear customer, this link 
                <a href="http://fcl-ecuador.com/Freighlogistics/main/chgpass.php?ref='.$user["id"].'&psw='.$cad.'"  target="_blank">Click Here</a>
                will allow you to log into your main page without the password, once you are there click on "My account"
                then choose  "Change password" and enter the new password information.
                                          
                </p>
                
                </body></html>
                ';
        //Para envío en formato HTML
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        //Dirección del remitente
        $headers .= "From: Freightlogistics <servicioclientes@fcl-ecuador.com>\r\n";
        mail($destinatario,$asunto,$cuerpo,$headers);


        echo "<script>alert('Usted recibirá un correo electrónico con un enlace para establecer una nueva contraseña.\\n\\nYou will receive an email with a link to set a new password.\\n');</script>";
        echo "<script>window.close();</script>";
        //echo 'http://principal-desarrollo.com/desarrollo/Freighlogistics/main/chgpass.php?ref='.$user["id"].'&psw='.$cad;

    }
}
?>

<!DOCTYPE html>

<html lang="es">

    <head>
        <meta charset="utf-8" />
        <link href="../css/reset.css" type="text/css" rel="stylesheet">   
        <link href="../css/layout.css" type="text/css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

        <!-- JSs -->
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/jquery.tinycarousel.js"></script>

        <link rel="shortcut icon" type="image/png" href="../img/favicon.png">
        <title>Freightlogistics</title>

    </head>

    <body>
    <div id="wrapper">



<!-- =================== HEADER  ====================== -->  


        <header id="h-main">
            <section id="top">
     
            </section>

            <section id="middle-forget">
            
            </section>

        </header>



<!-- =================== CONTENIDO  =================== -->         

        <div id="forget-content">
            <h1>Olvidé mi contraseña / Forgot Password</h1>
            <section id="forget-box">
                <form name="forget" method="post" action="#" <?php echo ($b)?"style='display:none":"" ?> >
                    <ul>
                        <li>Correo Electrónico / Email Address:</li>
                        <li><input type="email"  name="mail"><input type="submit" value="ENVIAR / SEND"></li>
                        <li>Se le enviará un enlace para restablecer tu contraseña.<br>You will be sent a link to reset your password.</li>
                    </ul>
                </form>
            </section>
            
        </div>

      <footer id="f-main">

            <section id="copyright">

            <ul>

                <li>Copyright © 2014 Freightlogistics C.A.</li>

                <!--  <li>Designed and developer by RF SOFTWARE SOLUTIONS</li>  -->

            </ul>

            </section>

        </footer>
    </body>
</html>
<?php

include_once("Consultas.php");
/********************************************************************************************************************/
/********************************************************************************************************************/
/********************************************************************************************************************/
function agregaBannerBig(){
    $con=new Consultas();
    $url=$_POST["url"];
    $imagen=$_FILES["file"]["name"];
    $activo=($_POST["activo"]!=null)?1:0;
    $ft=$_FILES["file"]["type"];
    $img_existe=true;

    if($_POST){
        if (($ft == "image/gif" || $ft == "image/jpeg" || $ft == "image/jpg" || $ft == "image/png"))
        {
            if (!$_FILES["file"]["error"] > 0)
            {
                if (!file_exists("../img/banner_big/" . $_FILES["file"]["name"])) {
                    move_uploaded_file($_FILES["file"]["tmp_name"],"../img/banner_big/" . $_FILES["file"]["name"]);
                    $img_existe=false;
                }
            }
            if(!$img_existe){
                $sql="INSERT INTO banner_big (imagen, url, activo) VALUES('$imagen','$url', '$activo');";
                //echo $sql;
                $con->grabar($sql);
            }else{
                $msg='El Archivo de imagen ya existe!';
            }
        }else{
            $msg='Error con la imagen del Banner.\\n\\nDeber cargar una archivo de imagen!';
            //echo "<script>alert()</script>";
        }
        if($msg!=null){
            echo "
            <script>
            alert('".$msg."');
            window.history.back();
            </script>";
        }
    }
}
function modificaBannerBig(){
    $con=new Consultas();
    $id=$_POST["id"];
    $url=$_POST["url"];
    $imagen=$_FILES["file"]["name"];
    $activo=($_POST["activo"]!=null)?1:0;
    $ft=$_FILES["file"]["type"];
    //$img_existe=true;
    $sql="UPDATE banner_big SET url='$url', activo='$activo' WHERE id='$id'";
    if($_POST){
        if (($ft == "image/gif" || $ft == "image/jpeg" || $ft == "image/jpg" || $ft == "image/png"))
        {
            if (!$_FILES["file"]["error"] > 0)
            {
                move_uploaded_file($_FILES["file"]["tmp_name"],"../img/banner_big/" . $_FILES["file"]["name"]);
                $sql="UPDATE banner_big SET imagen='$imagen', url='$url', activo='$activo' WHERE id='$id'";
            }
        }
        $con->grabar($sql);
    }
 }
function eliminaBannerBig(){
    $con=new Consultas();
    $id=$_POST["id"];
    $archivo = $con->nombre_archivo($id, "banner_big");
    echo "<script>".$archivo."</script>";
    //if(unlink("../img/banner_big/".$archivo)){
        $sql="DELETE FROM banner_big WHERE id='$id'";
        //echo $sql;
        $con->grabar($sql);
    //}






    //echo $sql;
    //$con->grabar($sql);
}

/********************************************************************************************************************/
/********************************************************************************************************************/
/********************************************************************************************************************/
function agregaBannerSmall(){
    $con=new Consultas();
    $url=$_POST["url"];
    $imagen=$_FILES["file"]["name"];
    $activo=($_POST["activo"]!=null)?1:0;
    $ft=$_FILES["file"]["type"];
    $img_existe=true;
    if($_POST){
        if (($ft == "image/gif" || $ft == "image/jpeg" || $ft == "image/jpg" || $ft == "image/png"))
        {
            if (!$_FILES["file"]["error"] > 0)
            {
                if (!file_exists("../img/banner_small/" . $_FILES["file"]["name"])) {
                    move_uploaded_file($_FILES["file"]["tmp_name"],"../img/banner_small/" . $_FILES["file"]["name"]);
                    $img_existe=false;
                }
            }
            if(!$img_existe){
                $sql="INSERT INTO banner_small (imagen, url, activo) VALUES('$imagen','$url', '$activo');";
                $con->grabar($sql);
            }else{
                $msg='El Archivo de imagen ya existe!';
            }
        }else{
            $msg='Error con la imagen del Banner.\\n\\nDeber cargar una archivo de imagen!';
        }
        if($msg!=null){
            echo "
            <script>
            alert('".$msg."');
            window.history.back();
            </script>";
        }
    }
}
function modificaBannerSmall(){
    $con=new Consultas();
    $id=$_POST["id"];
    $url=$_POST["url"];
    $imagen=$_FILES["file"]["name"];
    $activo=($_POST["activo"]!=null)?1:0;
    $ft=$_FILES["file"]["type"];
    //$img_existe=true;
    $sql="UPDATE banner_small SET url='$url', activo='$activo' WHERE id='$id'";
    if($_POST){
        if (($ft == "image/gif" || $ft == "image/jpeg" || $ft == "image/jpg" || $ft == "image/png"))
        {
            if (!$_FILES["file"]["error"] > 0)
            {
                move_uploaded_file($_FILES["file"]["tmp_name"],"../img/banner_small/" . $_FILES["file"]["name"]);
                $sql="UPDATE banner_small SET imagen='$imagen', url='$url', activo='$activo' WHERE id='$id'";

            }
        }
        //$sql="UPDATE banner_small SET imagen='$imagen', url='$url', activo='$activo' WHERE id='$id'";
        //echo $sql;
        $con->grabar($sql);
    }
}
function eliminaBannerSmall(){
    $con=new Consultas();
    $id=$_POST["id"];
    $archivo = $con->nombre_archivo($id, "banner_small");
   // if(unlink("../img/banner_small/".$archivo)){
        $sql="DELETE FROM banner_small WHERE id='$id'";
        //echo $sql;
        $con->grabar($sql);
    //}
}

/********************************************************************************************************************/
/********************************************************************************************************************/
/********************************************************************************************************************/
function agrega_noticias(){
    $con=new Consultas();
    $titulo=$_POST["tituloN"];
    $texto=$_POST["texto"];
    $fecha=$_POST["fecha"];
    $activo=($_POST["activo"]!=null)?1:0;

    $imagen=$_FILES["file"]["name"];
    $ft=$_FILES["file"]["type"];
    $img_existe=true;
    if($_POST){
        if (($ft == "image/gif" || $ft == "image/jpeg" || $ft == "image/jpg" || $ft == "image/png"))
        {
            if (!$_FILES["file"]["error"] > 0)
            {
                if (!file_exists("../img/news/" . $_FILES["file"]["name"])) {
                    move_uploaded_file($_FILES["file"]["tmp_name"],"../img/news/" . $_FILES["file"]["name"]);
                    $img_existe=false;
                }
            }
            if(!$img_existe){
                $sql="INSERT INTO news (titulo, texto, imagen, fecha, activo) VALUES('$titulo','$texto','$imagen','$fecha','$activo')";
                //echo $sql;
                $con->grabar($sql);
            }else{
                $msg='El Archivo de imagen ya existe!';
            }
        }else{
            $msg='Error con la imagen del Banner.\\n\\nDeber cargar una archivo de imagen!';
            //echo "<script>alert()</script>";
        }

        if($msg!=null){
            echo "
            <script>
            alert('".$msg."');
            window.history.back();
            </script>";
        }
    }
}

function modifica_noticias(){
    $con=new Consultas();
    $id=$_POST["id"];
    $titulo=$_POST["tituloN"];
    $texto=$_POST["texto"];
    $fecha=$_POST["fecha"];
    $activo=($_POST["activo"]!=null)?1:0;

    $imagen=$_FILES["file"]["name"];
    $ft=$_FILES["file"]["type"];
    $img_existe=true;
    $sql="UPDATE news SET titulo='$titulo', texto='$texto', fecha='$fecha', activo='$activo' WHERE id='$id';";
    if($_POST){
        if (($ft == "image/gif" || $ft == "image/jpeg" || $ft == "image/jpg" || $ft == "image/png"))
        {
            if (!$_FILES["file"]["error"] > 0)
            {
                move_uploaded_file($_FILES["file"]["tmp_name"],"../img/news/" . $_FILES["file"]["name"]);
                $sql="UPDATE news SET titulo='$titulo', texto='$texto', imagen='$imagen', fecha='$fecha', activo='$activo' WHERE id='$id';";
            }
        }
        //echo $sql;
        $con->grabar($sql);
    }
}


function elimina_noticias(){
    $con=new Consultas();
    $id=$_POST["id"];
    $archivo = $con->nombre_archivo($id, "news");
    echo "<script>".$archivo."</script>";
    unlink("../img/news/".$archivo);

    $sql="DELETE FROM news WHERE id='$id'";
    //echo $sql;
    $con->grabar($sql);
}

/********************************************************************************************************************/
/********************************************************************************************************************/
/********************************************************************************************************************/
function agrega_link(){

    $con=new Consultas();
    $grupo=$_POST["grupo"];
    $url=$_POST["url"];
    $desc=$_POST["desc"];
    $ventana=($_POST["ventana"]!=null)?1:0;
    $activo=($_POST["activo"]!=null)?1:0;
    $tipo=$_POST["tipo"];




    $imagen=$_FILES["file"]["name"];
    //$ft=$_FILES["file"]["type"];
    //$img_existe=true;
    //echo ">>>".$tipo;
    if($_POST){
        if($tipo=="archivo"){
            if (!$_FILES["file"]["error"] > 0)
            {
                //if (!file_exists("../img/news/" . $_FILES["file"]["name"])) {
                    move_uploaded_file($_FILES["file"]["tmp_name"],"../archivos/" . $_FILES["file"]["name"]);
                    //$img_existe=false;
                //}
            }
            //if(!$img_existe){
                $sql="INSERT INTO link_footer (grupo, url, descripcion, tipo, activo) VALUES('$grupo', '$imagen', '$desc', 'Archivo','$activo')";
                echo $sql;
                $con->grabar($sql);
            //}else{
            //    $msg='El Archivo de imagen ya existe!';
            //}
        }else if($tipo=="link"){
            $sql="INSERT INTO link_footer (grupo, url, descripcion, ventana, tipo, activo) VALUES('$grupo', '$url', '$desc', '$ventana', 'Link','$activo')";
            //echo $sql;
            $con->grabar($sql);
        }

        /*if($msg!=null){
            echo "
            <script>
            alert('".$msg."');
            window.history.back();
            </script>";
        }*/
    }
}


function modifica_link(){
    $con=new Consultas();
    $grupo=$_POST["grupo"];
    $url=$_POST["url"];
    $desc=$_POST["desc"];
    $ventana=($_POST["ventana"]!=null)?1:0;
    $activo=($_POST["activo"]!=null)?1:0;
    $tipo=$_POST["tipo"];
    $imagen=$_FILES["file"]["name"];
    $id=$_POST["id"];
    //$ft=$_FILES["file"]["type"];
    //$img_existe=true;
    if($_POST){

        if($tipo=="archivo"){
            echo ">>".$tipo;
            $sql="UPDATE link_footer SET grupo='$grupo', ventana='$ventana', tipo='Archivo', descripcion='$desc', activo='$activo' WHERE id='$id';";
            if (!$_FILES["file"]["error"] > 0)
            {
                //if (!file_exists("../img/news/" . $_FILES["file"]["name"])) {
                move_uploaded_file($_FILES["file"]["tmp_name"],"../archivos/" . $_FILES["file"]["name"]);
                $sql="UPDATE link_footer SET grupo='$grupo', url='$imagen', descripcion='$desc', ventana='$ventana', tipo='Archivo', activo='$activo' WHERE id='$id';";
                echo $sql.">>".$_FILES["file"]["size"];
                //$img_existe=false;
                //}
            }
            $con->grabar($sql);

        }else{
            //if(!$img_existe){
            //$sql="INSERT INTO link_footer (grupo, url, ventana, archivo, activo) VALUES('$grupo', '$url', '$ventana', '$imagen','$activo')";
            $sql="UPDATE link_footer SET grupo='$grupo', url='$url', descripcion='$desc', ventana='$ventana', tipo='Link', activo='$activo' WHERE id='$id';";
            //echo $sql;
            $con->grabar($sql);
            //}else{
            //    $msg='El Archivo de imagen ya existe!';
            //}
        }


        /*if($msg!=null){
            echo "
            <script>
            alert('".$msg."');
            window.history.back();
            </script>";
        }*/
    }
}
function elimina_link(){
    $con=new Consultas();
    $id=$_POST["id"];
    //$archivo = $con->nombre_archivo($id, "link_footer");
    //echo "<script>".$archivo."</script>";
    //unlink("../img/archivo/".$archivo);

    $sql="DELETE FROM link_footer WHERE id='$id'";
    //echo $sql;
    $con->grabar($sql);
}

/********************************************************************************************************************/
/********************************************************************************************************************/
/********************************************************************************************************************/
function agrega_usuario(){
    $con=new Consultas();

    $company_name=$_POST['company_name'];
    $contact_name=$_POST['contact_name'];
    $ruc=$_POST['ruc'];
    $address=$_POST['address'];
    $country=$_POST['country'];
    $city=$_POST['city'];
    $state=$_POST['state'];
    $zipcode=$_POST['zipcode'];
    $local_number=$_POST['local_number'];
    $mobile_number=$_POST['mobile_number'];
    $transpref01=$_POST['transpref01'];
    $transpref02=$_POST['transpref02'];
    $transpref03=$_POST['transpref03'];
    $ts_fb=(isset($_POST['ts_fb']))?1:0;
    $ts_fp=(isset($_POST['ts_fp']))?1:0;
    $ts_20=(isset($_POST['ts_20']))?1:0;
    $ts_40=(isset($_POST['ts_40']))?1:0;
    $ts_hq=(isset($_POST['ts_hq']))?1:0;
    $mail=strtolower($_POST['mail']);
    $pass=md5($_POST['pass']);
    $security_q=$_POST['security_q'];
    $security_a=$_POST['security_a'];
    $pr_01=$_POST['pr_01'];
    $pr_02=$_POST['pr_02'];
    $pr_03=$_POST['pr_03'];
    $pr_04=$_POST['pr_04'];
    $frqship=$_POST['frqship'];
    $frqship02=$_POST['frqship02'];
    $frqship03=$_POST['frqship03'];
    $frqship04=$_POST['frqship04'];

    $exist=$con->get_usuario($mail);

    if(count($exist)==0){

        $sql="INSERT INTO
              usuarios (company_name, contact_name, ruc, address, country, city, state, zipcode, local_number, mobile_number, transpref01, transpref02, transpref03, ts_fb, ts_fp, ts_20, ts_40, ts_hq, mail, pass, security_q, security_a, pr_01, pr_02, pr_03, pr_04, frqship01, frqship02, frqship03, frqship04)
              VALUES
              ('$company_name', '$contact_name', '$ruc', '$address', '$country', '$city', '$state', '$zipcode', '$local_number', '$mobile_number', '$transpref01', '$transpref02', '$transpref03', '$ts_fb', '$ts_fp', '$ts_20', '$ts_40', '$ts_hq', '$mail', '$pass', '$security_q', '$security_a', '$pr_01', '$pr_02', '$pr_03', '$pr_04', '$frqship','$frqship02' ,'$frqship03','$frqship04')";

        $con->grabar($sql);
        $reg=true;
    }else{
        echo "<script>alert('This e-mail address is already registered.')</script>";
        $reg=false;
    }
    return $reg;
}

function modifica_usuario($id){
    $con=new Consultas();
    $company_name=$_POST['company_name'];
    $contact_name=$_POST['contact_name'];
    $ruc=$_POST['ruc'];
    $address=$_POST['address'];
    $country=$_POST['country'];
    $city=$_POST['city'];
    $state=$_POST['state'];
    $zipcode=$_POST['zipcode'];
    $local_number=$_POST['local_number'];
    $mobile_number=$_POST['mobile_number'];
    $transpref01=$_POST['transpref01'];
    $transpref02=$_POST['transpref02'];
    $transpref03=$_POST['transpref03'];
    $ts_fb=(isset($_POST['ts_fb']))?1:0;
    $ts_fp=(isset($_POST['ts_fp']))?1:0;
    $ts_20=(isset($_POST['ts_20']))?1:0;
    $ts_40=(isset($_POST['ts_40']))?1:0;
    $ts_hq=(isset($_POST['ts_hq']))?1:0;
    $pr_01=$_POST['pr_01'];
    $pr_02=$_POST['pr_02'];
    $pr_03=$_POST['pr_03'];
    $pr_04=$_POST['pr_04'];
    $frqship=$_POST['frqship'];


    $sql="UPDATE usuarios
        SET
        company_name='$company_name',
        contact_name='$contact_name',
        ruc='$ruc',xx
        address='$address',
        country='$country',
        city='$city',
        state='$state',
        zipcode='$zipcode',
        local_number='$local_number',
        mobile_number='$mobile_number',
        transpref01='$transpref01',
        transpref02='$transpref02',
        transpref03='$transpref03',
        ts_fb='$ts_fb',
        ts_fp='$ts_fp',
        ts_20='$ts_20',
        ts_40='$ts_40',
        ts_hq='$ts_hq',
        pr_01='$pr_01',
        pr_02='$pr_02',
        pr_03='$pr_03',
        pr_04='$pr_04',
        frqship01='$frqship'
        WHERE id='$id'";
    $con->grabar($sql);
    //echo $sql;
}


function cambiar_password($mail,$pass){
    $con=new Consultas();
    $sql="UPDATE usuarios SET pass='$pass' WHERE mail='$mail'";
    $con->grabar($sql);
}

function elimina_usuario(){
    $con=new Consultas();
    $id=$_POST["id"];

    $sql="DELETE FROM usuarios WHERE id='$id'";
    //echo $sql;
    $con->grabar($sql);
}

/********************************************************************************************************************/
/********************************************************************************************************************/
/********************************************************************************************************************/
function agrega_contacto(){
    $con=new Consultas();

    $company_name=$_POST['company_name'];
    $client_name=$_POST['client_name'];
    $mail=strtolower($_POST['mail']);
    $phone=$_POST['phone'];
    $subject=$_POST['subject'];
    $message=$_POST['message'];
    $date_c= date("Y-m-d");
    $sql="INSERT INTO contact_us
          (company_name, client_name, mail, phone, subject, message, date_c)
          VALUES
          ('$company_name', '$client_name', '$mail', '$phone', '$subject', '$message', '$date_c')";
    //echo $sql;
    $con->grabar($sql);


    $destinatario = "ronaldflores001@gmail.com";
    $asunto = $subject;
    $cuerpo = '
                <html><body>
                <p>
                    Company Name: '.$company_name.'<br>
                    Contact Name: '.$client_name.'<br>
                    Name: '.$client_name.'<br>
                    Phone:'.$phone.'<br>
                    Date: '.$date_c.'<br><hr>
                </p>
                <p>'
        .$message.'
                </p>
                </body></html>
                ';
    //Para envío en formato HTML
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    //Dirección del remitente
    $headers .= "From: Freightlogistics <WEBMASTER@Freightlogistics.com>\r\n";
    mail($destinatario,$asunto,$cuerpo,$headers);




}
function elimina_contacto(){
    $con=new Consultas();
    $id=$_POST["id"];

    $sql="DELETE FROM contact_us WHERE id='$id'";
    //echo $sql;
    $con->grabar($sql);
}
/********************************************************************************************************************/
/********************************************************************************************************************/
/********************************************************************************************************************/
function agrega_quick_contact(){
    $con=new Consultas();
    $q_name=$_POST['q_name'];
    $q_phone=$_POST['q_phone'];
    $q_mail=strtolower($_POST['q_mail']);
    $q_text=$_POST['q_text'];
    $date_c= date("Y-m-d");
    $sql="INSERT INTO
          quick_contact(name, mail, phone, text, date_c)
          VALUES
          ('$q_name', '$q_mail', '$q_phone', '$q_text', '$date_c')";
    $con->grabar($sql);

    $destinatario = "victor.larrea@fcl-ecuador.com";
    $asunto = "Quick Contact";
    $cuerpo = '
                <html><body>
                <p>
                    Name: '.$q_name.'<br>
                    Phone:'.$q_phone.'<br>
                </p>
                <p>'
                    .$q_text.'
                </p>
                </body></html>
                ';
    //Para envío en formato HTML
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    //Dirección del remitente
    $headers .= "From: Freightlogistics <WEBMASTER@Freightlogistics.com>\r\n";
    mail($destinatario,$asunto,$cuerpo,$headers);

}

function elimina_quick_contact(){
    $con=new Consultas();
    $id=$_POST["id"];
    $sql="DELETE FROM quick_contact WHERE id='$id'";
    $con->grabar($sql);
}

/********************************************************************************************************************/
/********************************************************************************************************************/
/********************************************************************************************************************/
function agrega_newsletter(){
    $con=new Consultas();
    $n_mail=strtolower($_POST['n_mail']);
    //$date_c= date("Y-m-d");
    $sql="INSERT INTO newsletter
          (mail, activo)
          VALUES
          ('$n_mail', 1)";
    //echo $sql;
    $con->grabar($sql);
}

function elimina_newsletter(){
    $con=new Consultas();
    $id=$_POST["id"];
    $sql="DELETE FROM newsletter WHERE id='$id'";
    $con->grabar($sql);
}

/********************************************************************************************************************/
/********************************************************************************************************************/
/********************************************************************************************************************/
function agrega_admin(){
    $con=new Consultas();
    $cuenta=$_POST["cuenta"];
    $nombre=$_POST["name"];
    $mail=strtolower($_POST['mail']);
    $tipo=$_POST["tipo"];
    $pass=md5($_POST['$pass1']);
    if($_POST['pass1']!=""){
    $sql="INSERT INTO admin
          (cuenta, nombre, password, mail, tipo_usuario)
          VALUES
          ('$cuenta','$nombre','$pass','$mail','$tipo')";
    }

    $cuenta_exist=$con->get_admin_cuenta($cuenta);

    if(count($cuenta_exist)==0){
        $con->grabar($sql);
    }else{
        echo "<script>alert('User Account already exist!'); window.history.back()</script>";
    }
}

function modifica_admin(){
    $con=new Consultas();
    $id=$_POST['id'];
    $cuenta=$_POST["cuenta"];
    $nombre=$_POST["name"];
    $mail=strtolower($_POST['mail']);
    $tipo=$_POST["tipo"];
    $pass=md5($_POST['pass1']);



    if($_POST['pass1']!=""){
        $sql="UPDATE admin SET cuenta='$cuenta', nombre='$nombre', password='$pass', mail='$mail', tipo_usuario='$tipo' WHERE id=$id";

    }else{
        $sql="UPDATE admin SET cuenta='$cuenta', nombre='$nombre', mail='$mail', tipo_usuario='$tipo' WHERE id=$id";
    }
    $con->grabar($sql);
}






function elimina_admin(){
    $con=new Consultas();
    $id=$_POST["id"];
    $sql="DELETE FROM newsletter WHERE id='$id'";
    $con->grabar($sql);
}



/********************************************************************************************************************/
/********************************************************************************************************************/
/********************************************************************************************************************/
function agrega_air_shipment(){
    $con=new Consultas();
    $id_user=$_POST["id"];
    $awb=$_POST["awb_number"];
    $origen=$_POST["origen"];
    $from=$_POST["from"];
    $destino=$_POST["destino"];
    $numero_pck=$_POST["numero_pck"];
    $pick_up_date=$_POST["pick_up_date"];
    $sql="INSERT INTO air_tracking
          (id_user, awb_number, origen, shiped_from, destino,numero_pck, pick_up_date)
          VALUES
          ('$id_user','$awb','$origen','$from','$destino','$numero_pck','$pick_up_date')";
    if($con->validar_awb($awb)){
        $con->grabar($sql);
    }else{
        echo "<script>alert('AWB Number already exist!'); window.history.back()</script>";
    }
}


function elimina_air_detail($id){
    $con=new Consultas();
    $sql="DELETE FROM air_tracking_detail WHERE id='$id'";
    $con->grabar($sql);
}

function agrega_air_detail(){
    $con=new Consultas();
    $dep_airport=$_GET["dep_airport"];
    $dep_date=$_GET["dep_date"];
    $arr_airport=$_GET["arr_airport"];
    $arr_date=$_GET["arr_date"];
    $awb=$_GET["awb_number"];
    $sql="INSERT INTO air_tracking_detail
          (awb_number, dep_airport, dep_date, arr_airport, arr_date)
          VALUES
          ('$awb','$dep_airport','$dep_date','$arr_airport','$arr_date')";
    $con->grabar($sql);
}

function update_air_detail(){
    $con=new Consultas();
    $dep_airport=$_GET["dep_airport"];
    $dep_date=$_GET["dep_date"];
    $arr_airport=$_GET["arr_airport"];
    $arr_date=$_GET["arr_date"];
    $dep_airport=$_GET["dep_airport"];
    $id=$_GET["id"];
    $sql="UPDATE air_tracking_detail
          SET
              dep_airport = '$dep_airport',
              dep_date='$dep_date',
              arr_airport='$arr_airport',
              arr_date='$arr_date'
          WHERE  id='$id'";
    $con->grabar($sql);
}

function update_air_shipment(){
    $con=new Consultas();
    $id_user=$_POST["id"];
    $awb=$_POST["awb_number"];
    $origen=$_POST["origen"];
    $from=$_POST["from"];
    $destino=$_POST["destino"];
    $numero_pck=$_POST["numero_pck"];
    $pick_up_date=$_POST["pick_up_date"];
    $sql="UPDATE air_tracking
          SET
              origen='$origen',
              shiped_from='$from',
              destino='$destino',
              numero_pck='$numero_pck',
              pick_up_date='$pick_up_date'
          WHERE awb_number='$awb'";

    $con->grabar($sql);
}



/********************************************************************************************************************/
/********************************************************************************************************************/
/********************************************************************************************************************/
function agrega_sea_shipment(){
    $con=new Consultas();
    $id_user=$_POST["id"];
    $bl=$_POST["bl_number"];
    $type_c=$_POST["type_c"];
    $origen=$_POST["origen"];
    $to=$_POST["shipped_to"];
    $destino=$_POST["destino"];
    $final_pod=$_POST["final_pod"];
    $eta=$_POST["eta"];
    //$pick_up_date=$_POST["pick_up_date"];
    $sql="INSERT INTO sea_tracking
          (id_user, bl_number, type, origen, shipped_to, destino,final_pod, eta)
          VALUES
          ('$id_user','$bl','$type_c','$origen','$to','$destino','$final_pod','$eta')";
    //echo $sql;
    if($con->validar_bl($bl)){
        $con->grabar($sql);
    }else{
        echo "<script>alert('BL Number already exist!'); window.history.back()</script>";
    }
}

function elimina_sea_detail($id){
    $con=new Consultas();
    $sql="DELETE FROM sea_tracking_detail WHERE id='$id'";
    $con->grabar($sql);
}

function agrega_sea_detail(){
    $con=new Consultas();
    $location=$_GET["location"];
    $description=$_GET["description"];
    $date=$_GET["date"];
    $vessel=$_GET["vessel"];
    $voyage=$_GET["voyage"];
    $bl=$_GET["bl_number"];
    $sql="INSERT INTO sea_tracking_detail
          (bl_number, location, description, date, vessel, voyage)
          VALUES
          ('$bl','$location','$description','$date','$vessel','$voyage')";
    $con->grabar($sql);
}


function update_sea_detail(){
    $con=new Consultas();
    $location=$_GET["location"];
    $description=$_GET["description"];
    $date=$_GET["date"];
    $vessel=$_GET["vessel"];
    $voyage=$_GET["voyage"];
    $id=$_GET["id"];
    $sql="UPDATE sea_tracking_detail
          SET
              location = '$location',
              description='$description',
              date='$date',
              vessel='$vessel',
              voyage='$voyage'
          WHERE  id='$id'";
    $con->grabar($sql);
}

function update_sea_shipment(){
    $con=new Consultas();
    $id_user=$_POST["id"];
    $bl=$_POST["bl_number"];
    $type_c=$_POST["type_c"];
    $origen=$_POST["origen"];
    $to=$_POST["shipped_to"];
    $destino=$_POST["destino"];
    $final_pod=$_POST["final_pod"];
    $eta=$_POST["eta"];
    //$pick_up_date=$_POST["pick_up_date"];
    $sql="UPDATE sea_tracking
          SET
              type='$type_c',
              origen='$origen',
              shipped_to='$to',
              destino='$destino',
              final_pod='$final_pod',
              eta='$eta'
          WHERE bl_number='$bl'";
    //echo $sql;
    $con->grabar($sql);
}

/*****************************************************************************************************/
/*****************************************************************************************************/
/*****************************************************************************************************/

function lang($eng, $esp){
    $lang="eng";
    if(isset($_SESSION["idioma"])){
        $lang=$_SESSION["idioma"];
    }
    if($lang=="esp"){
        echo $esp;
    }else{
        echo $eng;
    }
}

function date_to_mysql($date)
{
    $d = substr($date, 6, 4) . "-" . substr($date, 3, 2) . "-" . substr($date, 0, 2);
    return $d;

}




?>
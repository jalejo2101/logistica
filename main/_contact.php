<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php

require_once '../include/header.php';

if($_POST){
    echo(">>>>>>>>");
    agrega_contacto();

}


?>
<script>
    $(document).ready(function() {
        $('nav#menu-top ul.main-sect > li:nth-child(6) a').addClass("page-on");
    });
</script>
<!-- =================== CONTENIDO  =================== -->         

        <div id="content">

            <?php
                require_once '../include/aside.php';
            ?>

            <div id="main-content" class="noaliados">

                <div class="white-box-small">
					
                    <h2>CONTACT US</h2>						
				    
                    <form id="contact-us" method="post" name="contact-us">
                        <h4>Company Name</h4>
                        <input type="text" name="company_name" >
                        <h4>Name</h4>
                        <input type="text" name="name_user" >
                        <h4>Email <span>*</span></h4>
                        <input type="email" name="mail" required="required" oninvalid="this.setCustomValidity('Please enter your Email')">
                        <h4>Phone</h4>
                        <input type="tel" name="phone">
                        <h4>Subject</h4>
                        <select>
                            <option>Option A</option>
                            <option>Option B</option>
                            <option>Option C</option>
                            <option>Option D</option>
                        </select>
                        <h4>Message<span>*</span></h4>
                        <textarea placeholder="Enter your message here..." name="message">
                        </textarea>
                        <input type="submit" value="Send Inquiry">
                    </form>
                    
                    <aside class="mini-aside">
                        <h6>Call Us</h6>
                        <ul>
                            <li>Office: 593-4-6015076</li>
                            <li>Mobile: 593-992836034</li>
                            <li>AOH: 593-99</li>
                        </ul>
                    </aside>
                    <img class="map" src="../img/map.png">
                </div>

                
                <!--
                <div id="logos">
                    <div class="viewport">
                        <ul class="overview">
                            <li>
                                <a><img src="../img/logo01.jpg"></a>
                                <a><img src="../img/logo02.png"></a>
                                <a><img src="../img/logo03.png"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <script type="text/javascript">
                        $(document).ready(function(){
                            $("#logos").tinycarousel({
                                    bullets  : true, interval  : true
                            });
                        });
                </script>
                -->
            </div>
        </div>


<!-- =================== FOOTER  ====================== -->   

<?php
    require_once '../include/footer.php';
?>
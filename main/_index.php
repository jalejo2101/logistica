<!DOCTYPE html>
<html lang="es">

    <head>
    
        <meta charset="utf-8" />
        
        <link href="../css/reset.css" type="text/css" rel="stylesheet">   
        <link href="../css/layout.css" type="text/css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        
        <!-- JSs -->
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/jquery.tinycarousel.js"></script>
        
        <title>Freightlogistics</title>
    </head>
           
    <body>
    <script>
    $(document).ready(function() {
        $('nav#menu-top ul.main-sect > li:nth-child(1) a').addClass("page-on");
    });
    
    function show(mostrar){
        $('.darkbox-cont').fadeIn();
        $(mostrar).fadeIn();
    }
        
    function cierra(){
        $('.darkbox-cont').fadeOut();
        $('.login-box').fadeOut();
        $('.why-box').fadeOut();
        $('.quick-box').fadeOut();
    }
    </script>
        
    <div class="darkbox-cont">
        <section class="login-box">
            <h2>LOGIN</h2>
            <ul>
                <li><label>Email</label><input type="email" name="email" placeholder="Email"></li>
                <li><label>Password</label><input type="password" name="password" placeholder="Password"></li>
                <li><input type="checkbox" name="remember" value="remember">Remember me</li>
            </ul>
            <div>
                <input type="submit" value="LOGIN">
                <input type="button" value="CANCEL" onclick="cierra();">
                
            </div>
        </section>
        
        <section class="why-box">
            <a href="#" onclick="cierra();"></a>
            <h2>WHY FCL</h2>
            <p>
                Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500,
            </p>
            <p>
                Al contrario del pensamiento popular, el texto de Lorem Ipsum no es simplemente texto aleatorio. Tiene sus raices en una pieza cl´sica de la literatura del Latin, que data del año 45 antes de Cristo, haciendo que este adquiera mas de 2000 años de antiguedad. Richard McClintock, un profesor de Latin de la Universidad de Hampden-Sydney en Virginia
            </p>
        </section>
        
        <section class="quick-box">
            <h2>QUICK</h2>
            <ul>
                <li><label>Nombre</label><input type="text" name="nombre" placeholder="Email"></li>
                <li><label>Email</label><input type="email" name="email" placeholder="Password"></li>
                <li><label>Phone</label><input type="tel" name="phone" placeholder="Password"></li>
                <li><label>Message</label><textarea></textarea>
            </ul>
            <div>
                <input type="submit" value="SUBMIT">
                <input type="button" value="CANCEL" onclick="cierra();">
                
            </div>
        </section>
        
    </div>
        
    <div id="wrapper">
<!-- =================== HEADER  ====================== -->  
    
        
    
        <header id="h-main">
            <section id="top">
                <div>
                    <ul class="sociales">
                        <li><a href="#"><img src=../img/soc-twitter.png></a></li>
                        <li><a href="#"><img src=../img/soc-face.png></a></li>
                        <li><a href="#"><img src=../img/soc-skype.png></a></li>
                        <li><a href="#"><img src=../img/soc-gplus.png></a></li>
                    </ul>
                    <ul class="idiomas">
                        <li><a href="#"><img src=../img/len-esp-on.png></a></li>
                        <li><a href="#"><img src=../img/len-ing-on.png></a></li>
                    </ul>
                </div>
            </section>
            <section id="middle">
                <div class="mini-menu">
                    <a class="why" onclick="show('.why-box');">Why FCL</a>
                    <ul class="login">
                        <li>Welcome to Freightlogistics</li>
                        <li><a href="#" onclick="show('.login-box');">Login</a></li>
                        <li><a href="#">Register</a></li>
                    </ul>
                    <ul class="account">
                        <li><a href="#">My Account</a></li>
                        <li><a href="#">Add to Favorites</a></li>
                    </ul>
                </div>
            </section>
            <nav id="menu-top">
                <ul class="main-sect">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">About us</a></li>
                    <li><a href="#">Services </a>
                        <ul>
                            <li><a href="#">Internation Transportation</a></li>
                            <li><a href="#">Pick up / Delivery</a></li>
                            <li><a href="#">Custom Brokerage</a></li>
                            <li><a href="#">Cargo Insurance</a></li>
                        </ul>
                    </li>
                    <li><a href="#">How It Works</a></li>
                    <li><a href="#">Contact us</a></li>
                    <li></li>
                </ul>
            </nav>
            <section id="banner">
                    <div id="banner-main">
                        <div class="viewport">
                            <ul class="overview">
                                <li><img src="../img/slider-01.png"></li>
                                <li><img src="../img/slider-02.png"></li>
                                <li><img src="../img/slider-03.png"></li>
                                <li><img src="../img/slider-04.png"></li>
                            </ul>
                        </div>
                        <ul class="bullets">
                            <li><a href="#" class="bullet active" data-slide="0"></a></li>
                            <li><a href="#" class="bullet" data-slide="1"></a></li>
                            <li><a href="#" class="bullet" data-slide="2"></a></li>
                            <li><a href="#" class="bullet" data-slide="3"></a></li>
                
                        </ul>

                        <script type="text/javascript">
                            $(document).ready(function()
                            {
                                $("#banner-main").tinycarousel({
                                    bullets  : true, interval  : true
                                });
                            });
                        </script>
                    </div>
            </section>
        </header>

<!-- =================== CONTENIDO  =================== -->         
        <div id="content">
            
            <aside id="lateral">
                <section>
                   <h4>Track Shipment</h4>
                   <form>
                       <input type="text" name="track" id="track" placeholder="AWB/CONTAINER NUMBER">
                       <input type="submit" value=" " class="fsearch">
                    </form>
                </section>
                <section>
                   <h4>E-mail Newsletter</h4>
                    <form>
                       <input type="text" name="track" id="track" placeholder="Email Address">
                       <input type="submit" value=" " class="fmail">
                    </form>
                </section>
                <section>
                   <h4>Latest News</h4>
                    <article class="news">
                        <aside><header>MAY</header><div>05</div></aside>
                        <h5>Lorem Ipsum</h5>
                        <p>es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500</p>
                    </article>
                    <article class="news">
                        <aside><header>MAY</header><div>05</div></aside>
                        <h5>Lorem Ipsum</h5>
                        <p>es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem</p>
                    </article>
                    <a class="allnews" href="#">All news <span> > </span> </a>
                </section>
                <section>
                   <h4 class="cursor-link"  onclick="show('.quick-box');">Quick Contact</h4>
                </section>
            </aside>
            <div id="main-content">
                <nav>
                    <ul>
                        <li><a href="#">Sea Freight</a></li>
                        <li><a href="#">air cargo</a></li>
                        <li><a href="#">pick up & delivery</a></li>
                        <li><a href="#">customs</a></li>
                    </ul>
                </nav>
                <!--
                <div class="grey-box">
                    <h2>Shipping Insurance</h2>
                    <p>
                    Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) 
                    </p>
                    <p>
                    Hay muchas variaciones de los pasajes de Lorem Ipsum disponibles, pero la mayoría sufrió alteraciones en alguna manera, ya sea porque se le agregó humor, o palabras aleatorias que no parecen ni un poco creíbles. Si vas a utilizar un pasaje de Lorem Ipsum, necesitás estar seguro de que no hay nada avergonzante escondido en el medio del texto. 
                    </p>
                    <p>
                    Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) 
                    </p>
                    <p>
                    Hay muchas variaciones de los pasajes de Lorem Ipsum disponibles, pero la mayoría sufrió alteraciones en alguna manera, ya sea porque se le agregó humor, o palabras aleatorias que no parecen ni un poco creíbles. Si vas a utilizar un pasaje de Lorem Ipsum, necesitás estar seguro de que no hay nada avergonzante escondido en el medio del texto. 
                    </p>
                </div>
-->
                <div class="grey-box">
                    <form>
                        <h5>ONLINE FREIGHT QUOTE</h5>
                        <h5>CHOOSE THE BEST RATE FOR YOUR CARGO</h5>
                        <ul class="BestRate">
                            <li><input type="radio" name="best-rate" value="sea"><label>sea</label></li>
                            <li><input type="radio" name="best-rate" value="air"><label>air</label></li>
                            <li><input type="radio" name="best-rate" value="household"><label>household</label></li>
                            <li><input type="radio" name="best-rate" value="export"><label>export</label></li>
                            <li><input type="radio" name="best-rate" value="import"><label>import</label></li>
                        </ul>
                        <h5>METHOD OF SHIPMENT</h5>
                        <ul class="MethodShipment">
                            <li>
                                <p>ORIGIN</p>
                                <select>
                                    <option>Miami</option>
                                    <option>Venezuela</option>
                                    <option>Panama</option>
                                    <option>Colombia</option>
                                </select>
            
                                <p>ZIPCODE</p>
                                <input type="text" name="origin-zip" placeholder="ZIPCODE">
                            </li>
                            
                            <li>
                                <p>DESTINATION</p>
                                <select>
                                    <option>Miami</option>
                                    <option>Venezuela</option>
                                    <option>Panama</option>
                                    <option>Colombia</option>
                                </select>
            
                                <p>ZIPCODE</p>
                                <input type="text" name="destination-zip" placeholder="ZIPCODE">
                            </li>
                        </ul>
                        <input type="submit" value="">
                    </form>
                </div>
                <section class="logos"><img src="../img/logos.png"></section>
            </div>
        
        </div>

<!-- =================== FOOTER  ====================== -->   
        
        <footer id="f-main">
            <nav id="menu-bottom">
                <section>
                    <h3>POPULAR LIKNS</h3>
                    <ul>
                        <li><a href="http://www.aduana.gob.ec/index.action" target="_blank">	ECUADORIAN CUSTOMS	</a></li>
                        <li><a href="http://www.industrias.gob.ec/" target="_blank">	Ministry of Industry and Productivity	</a></li>
                        <li><a href="http://www.normalizacion.gob.ec/" target="_blank">	Ecuadorian Standards Institute - INEN	</a></li>
                        <li><a href="http://www.acreditacion.gob.ec/" target="_blank">	Ecuadorian Accreditation Organization	</a></li>
                        <li><a href="http://www.cgsa.com.ec/inicio.aspx" target="_blank">	PORT TERMINAL CONTECON	</a></li>
                        <li><a href="http://www.aretina.com/index.php?option=com_content&view=article&id=31&Itemid=52" target="_blank">	PORT TERMINAL BANAPUERTO	</a></li>
                        <li><a href="http://www.tpg.com.ec/" target="_blank">	PORT TERMINAL TPG	</a></li>
                    </ul>
                </section>
                <section>
                    <h3>TOOLS</h3>
                    <ul>
                        <li><a href="http://www.softtruck.com/?source=One_Phrase&gclid=CIWgqKmfjL4CFYqhOgodsRQACg" target="_blank">	CONTAINER CALCULATOR	</a></li>
                        <li><a href="http://www.unitconverters.net/" target="_blank">	MEASUREMENT CONVERTER	</a></li>
                        <li><a href="http://www.customs.gov.au/webdata/resources/files/Incoterms.pdf" target="_blank">	INCOMTERMS	</a></li>
                        <li><a href="http://www.acuitivesolutions.com/Calculator/ChargeableWeight.aspx" target="_blank">	AIR CARGO CALCULATOR	</a></li>
                        <li><a href="http://www.dutycalculator.com/new-import-duty-and-tax-calculation/" target="_blank">	DUTY CALCULATOR	</a></li>
                        <li><a href="http://www.aduana.gob.ec/pro/household_goods.action" target="_blank">	HOUSEHOLDS TO ECUADOR 	</a></li>
                        <li><a href="http://www.dutycalculator.com/country-guides/Import-duty-taxes-when-importing-into-Ecuador/" target="_blank">	HOW TO IMPORT TO ECUADOR	</a></li>
                        <li><a href="http://www.aduana.gob.ec/pro/for_travelers.action" target="_blank">	FOR TRAVELER	</a></li>                    
                    </ul>
                </section>
                <section>
                    <h3>ABOUT US</h3>
                    <ul>
                        <li><a href="http://www.flc-ecuador.com/" target="_blank">	WHO WE ARE	</a></li>
                        <li><a href="#" target="_blank">PRIVATE POLICY</a></li>
                        <li><a href="#" target="_blank">TERMS & CONDITIONS</a></li>
                        <li><a href="#" target="_blank">CONTACT US</a></li>
                    </ul>
                </section>
                <section>
                    <h3>SUPPORT</h3>
                    <ul>
                        <li><a href=" " target="_blank">FRECUENTLY ASKED QUESTION</a></li>
                        <li><a href=" " target="_blank">MUDANZAS</a></li>
                        <li><a href=" " target="_blank">INSTRUCCIONES DE EMBARQUE</a></li>
                        <li><a href=" " target="_blank">DOCUMENTACION</a></li>
                        <li><a href=" " target="_blank">RESERVA</a></li>
                        <li><a href=" " target="_blank">CONSEJO PARA LA IMPORTACION</a></li>
                        <li><a href=" " target="_blank">CONSEJO PARA LA EXPORTACION</a></li>                
                    </ul>
                </section>
                <!--
                <section>
                    <h3>SOCIAL</h3>
                    <ul>
                        <li><a href="https://www.facebook.com/pages/World-Freight-Rates/442892302431975?fref=ts" target="_blank">Facebook</a></li>
                        <li><a href="https://twitter.com/WFRates" target="_blank">Twitter</a></li>
                        <li><a href="#" target="_blank">SKYPE</a></li>
                        <li><a href="#" target="_blank">GOOGLE +</a></li>
                    </ul>
                </section>
            -->
            </nav>
            <section id="copyright"></section>
        </footer>
    </body>
   </div> 
</html>
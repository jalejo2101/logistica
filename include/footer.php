<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php
$con=new Consultas();
$lg=($_SESSION["idioma"]=="esp")?"_esp":"";
$lst_pl=$con->get_lst_grupo("POPULAR LIKNS", $lg);
$lst_tl=$con->get_lst_grupo("TOOLS", $lg);
$lst_au=$con->get_lst_grupo("ABOUT US", $lg);
$lst_su=$con->get_lst_grupo("SUPPORT", $lg);

?>

    <footer id="f-main">
            <nav id="menu-bottom">
                <section>
                    <h3><?php lang("POPULAR LIKNS","ENLACES POPULARES")?></h3>
                    <ul>
                        <?php foreach($lst_pl as $item){ ?>
                            <?php $fl=($item["tipo"]=="Archivo")?"../archivos/":""?>
                            <li><a href="<?php echo $fl.$item['url'] ?>" <?php echo ($item['ventana']==1)?"target='_blank'":"";?>><?php echo $item['descripcion'] ?></a></li>
                        <?php }?>
                    </ul>
                </section>
                <section>
                    <h3><?php lang("TOOLS","HERRAMIENTAS")?></h3>
                    <ul>
                        <?php foreach($lst_tl as $item){?>
                            <?php $fl=($item["tipo"]=="Archivo")?"../archivos/":""?>
                            <li><a href="<?php echo $fl.$item['url'] ?>" <?php echo ($item['ventana']==1)?"target='_blank'":"";?>><?php echo $item['descripcion'] ?></a></li>
                        <?php }?>
                    </ul>
                </section>
                <section>
                    <h3><?php lang("ABOUT US","COMPAÑIA")?></h3>
                    <ul>
                        <?php foreach($lst_au as $item){ ?>
                            <?php $fl=($item["tipo"]=="Archivo")?"../archivos/":""?>
                            <li><a href="<?php echo $fl.$item['url'] ?>" <?php echo ($item['ventana']==1)?"target='_blank'":"";?>><?php echo $item['descripcion'] ?></a></li>
                        <?php }?>
                    </ul>
                </section>
                <section>
                    <h3><?php lang("SUPPORT","SOPORTE")?></h3>
                    <ul>
                        <?php foreach($lst_su as $item){ ?>
                            <?php $fl=($item["tipo"]=="Archivo")?"../archivos/":""?>
                            <li><a href="<?php echo $fl.$item['url'] ?>" <?php echo ($item['ventana']==1)?"target='_blank'":"";?>><?php echo $item['descripcion'] ?></a></li>
                        <?php }?>
                    </ul>
                </section>
            </nav>
            <section id="copyright">
            <ul>
                <li>Copyright © 2014 Freightlogistics C.A.</li>
                <!--  <li>Designed and developer by RF SOFTWARE SOLUTIONS</li>  -->
            </ul>
            </section>
        </footer>
   </div> 

 </body>

</html>